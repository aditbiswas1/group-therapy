/**
 * Model definitions
 *
 */

Workspace = Model.create("workspaces", {
    name$: null,
    description: null
});

Project = Model.create("projects", {
        name$: null,
        description: null
    }
);

Team = Model.create("teams", {
        name$: null,
        description: null
});

User = Model.create("users", {
        name$: null,
        description: null
});

Task = Model.create("tasks", {
        name$: null,
        description: null
});

Message = Model.create("messages", {
        name$: null,
        description: null
});

Workspace.hasMany(Project);
Project.hasMany(User);
Task.hasMany(Message);
Project.hasMany(Task);
Task.hasMany(User, {plural: "assignees"});
